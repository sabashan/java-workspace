package rh.sp.q13;

import rh.sp.q12.TriangleValidity;

public class RightAngle {
	public static void main(String[] args) {

		double sideA = 4.0;
		double sideB = 10.0;
		double sideC = 5.0;

		boolean isValidTriangle = TriangleValidity.isValidTriangle(sideA, sideB, sideC);

		if (isValidTriangle) {
			System.out.println("ABC is a valid triangle");
			boolean status = isRightAngleTriangle(sideA, sideB, sideC);
			if (status) {
				System.out.println("ABC is a valid right angle triangle");
			} else {
				System.out.println("ABC is not a valid right angle triangle");
			}
		} else {
			System.out.println("ABC is not a valid triangle");
		}

	}

	public static boolean isRightAngleTriangle(double sideA, double sideB, double sideC) {

		double ab = Math.pow(sideA, 2) + Math.pow(sideB, 2);
		double ac = (sideA * sideA) + (sideC * sideC);
		double bc = Math.pow(sideB, 2) + Math.pow(sideC, 2);

		if ((Math.pow(sideA, 2) == bc) || (Math.pow(sideB, 2) == ac) || (Math.pow(sideC, 2) == ab)) {
			return true;
		}
		return false;
	}
}
