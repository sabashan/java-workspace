package rh.sp.q4;

public class Slashes {

	static final String pattern = "/";
	static final int designPerLine = 122;
	static final int numberofLines = 10;

	public static void printSlashes() {

		for (int i = 1; i <= designPerLine; i++) {
			System.out.print(pattern + " ");
		}
		System.out.println();
		for (int i = 1; i < designPerLine; i++) {
			System.out.print(" " + pattern);
		}
		System.out.println();
	}
	
	public static void printPattern() {
		for (int i = 0; i < numberofLines/2; i++) {
			Slashes.printSlashes();
		}
	}
	
	public static void main(String[] args) {
		Slashes.printPattern();
	}

}
