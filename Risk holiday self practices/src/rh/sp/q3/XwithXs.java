package rh.sp.q3;

public class XwithXs {
	final String space = "-";
	final String pattern = "X";
	int lineNumber = 5;

	public static void main(String[] args) {
		XwithXs xwx = new XwithXs();
		xwx.printLower();
		xwx.printUpper();		
	}

	public void printUpper() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= lineNumber - line; sp++) {
				System.out.print(space);
			}
			System.out.print(pattern);

			for (int sp = 1; sp <= (2 * line) - 2; sp++) {
				System.out.print(space);
			}
			System.out.println(pattern);
		}
	}

	public void printLower() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= line - 1; sp++) {
				System.out.print(space);
			}
			System.out.print(pattern);

			for (int sp = 1; sp <= (lineNumber * 2) - (2 * line); sp++) {
				System.out.print(space);
			}
			System.out.println(pattern);
		}
	}
}
