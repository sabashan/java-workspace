package rh.sp.q1;

import rh.sp.in.UserInput;

public class Diamond {

	final String space = " ";
	final String fowardSlash = "/";
	final String backwardSlash = "\\";
	private static int lineNumber = 0;

	public void printUpperTriangle() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= lineNumber - line; sp++) {
				System.out.print(space);
			}
			System.out.print(fowardSlash);

			for (int sp = 1; sp <= (2 * line) - 2; sp++) {
				System.out.print(space);
			}
			System.out.println(backwardSlash);
		}
	}

	public void printLowerTriangle() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= line - 1; sp++) {
				System.out.print(space);
			}
			System.out.print(backwardSlash);

			for (int sp = 1; sp <= (lineNumber - line) * 2; sp++) {
				System.out.print(space);
			}
			System.out.println(fowardSlash);
		}
	}

	public static void main(String[] args) {
		Diamond diamond = new Diamond();
		lineNumber = UserInput.readNumber("Enter the line number");
		diamond.printUpperTriangle();
		diamond.printLowerTriangle();
	}
}


