package rh.sp.q2;

public class DiamondFilled {
	final String space = ".";
	final String fowardSlash = "/";
	final String backwardSlash = "\\";
	int lineNumber = 5;

	public static void main(String[] args) {
		DiamondFilled filledDiamond = new DiamondFilled();
		filledDiamond.printFilledUpperTriangle();
		filledDiamond.printFilledLowerTriangle();
	}

	public void printFilledUpperTriangle() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= lineNumber - line; sp++) {
				System.out.print(space);
			}
			for (int fs = 1; fs <= line; fs++) {
				System.out.print(fowardSlash);
			}
			for (int fs = 1; fs <= line; fs++) {
				System.out.print(backwardSlash);
			}
			System.out.println();
		}
	}

	public void printFilledLowerTriangle() {
		for (int line = 1; line <= lineNumber; line++) {
			for (int sp = 1; sp <= line - 1; sp++) {
				System.out.print(space);
			}
			for (int sp = 1; sp <= lineNumber - line + 1; sp++) {
				System.out.print(backwardSlash);
			}
			for (int fs = 1; fs <= lineNumber - line + 1; fs++) {
				System.out.print(fowardSlash);
			}
			System.out.println();
		}
	}
}


//Expected output: Number of lines 5
/*
    /\
   //\\
  ///\\\
 ////\\\\
/////\\\\\
\\\\\/////
 \\\\////
  \\\///
   \\//
    \/

*/

//Actual output:

/*
..../\
...//\\
..///\\\
.////\\\\
/////\\\\\
\\\\\/////
.\\\\////
..\\\///
...\\//
....\/
*/