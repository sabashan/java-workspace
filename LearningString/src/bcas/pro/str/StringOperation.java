package bcas.pro.str;

public class StringOperation {

	public char getCharAt(int index, String word) {
		char ch = word.charAt(index);
		return ch;
	}

	public String getToUpperCase(String word) {
		String str = word.toUpperCase();
		return str;
	}

	public String getToLowerCase(String word) {
		return word.toLowerCase();
	}
	
	public int getIndexOf(char ch,int fromIndex,String para) {
		return para.indexOf('a',fromIndex);
	}
	
	public int getIndexOfString(String str,String para) {
		return para.indexOf(str);
	}
}
