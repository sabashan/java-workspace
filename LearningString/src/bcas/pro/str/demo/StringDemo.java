package bcas.pro.str.demo;

import bcas.pro.str.StringOperation;

public class StringDemo {

	public static void main(String[] args) {

		String campus = "BCAS";
		String name = "Sabashan";
		String address = "Colombo";
		
		String para="Java's IS a general-purpose programming language that is class-based, "
				+ "object-oriented, and designed to have as few implementation dependencies as "
				+ "possible. It is intended to let application developers write once, run anywhere"
				+ " (WORA), meaning that compiled Java code can run on all platforms that support"
				+ " Java without the need for recompilation.  applications are typically "
				+ "compiled to bytecode that can run on any Java virtual machine (JVM) regardless"
				+ " of the underlying computer architecture. The syntax of Java is similar to "
				+ "C and C++, but it has fewer low-level facilities than either of them. ";
				
		StringOperation opp = new StringOperation();
		//System.out.println(opp.getCharAt(2, campus));
		
		//System.out.println(opp.getToUpperCase(name));
		//System.out.println(opp.getToLowerCase(address));
		
//		System.out.println(opp.getIndexOf('a', 1000, para));
		
		String serchText="IS";
		System.out.println(opp.getIndexOfString(serchText, para));

		

	}
}


